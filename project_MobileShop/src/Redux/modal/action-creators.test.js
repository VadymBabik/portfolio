import * as types from "./actions";
import * as aCreator from "./action-creators";
import { idModalProduct, toggleShowModal } from "./action-creators";

describe("Test for action-creator modalReducer", () => {
  it("toggleShowModal", () => {
    expect(aCreator.toggleShowModal()).toEqual({
      type: types.MODAL_SHOW,
    });
  });
  it("idModalProduct", () => {
    expect(aCreator.idModalProduct(1)).toEqual({
      type: types.MODAL_ID_PRODUCT,
      payload: 1,
    });
  });
});
