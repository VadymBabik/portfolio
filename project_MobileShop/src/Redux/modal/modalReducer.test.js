import { modalReducer } from "./modalReducer";
import { MODAL_SHOW, MODAL_ID_PRODUCT } from "./actions";

describe("Test for modalReducer", () => {
  it("MODAL_SHOW", () => {
    const initialState = {
      isModal: false,
      idModalProduct: null,
    };
    const action = {
      type: MODAL_SHOW,
    };
    const reducer = modalReducer(initialState, action);
    expect(reducer.isModal).toEqual(!initialState.isModal);
  });

  it("MODAL_ID_PRODUCT", () => {
    const initialState = {
      isModal: false,
      idModalProduct: null,
    };
    const action = {
      type: MODAL_ID_PRODUCT,
      payload: 2,
    };

    const reducer = modalReducer(initialState, action);
    expect(reducer.idModalProduct).toEqual(2);
  });
  it("undefined", () => {
    const initialState = {
      isModal: false,
      idModalProduct: null,
    };
    expect(modalReducer(initialState, {})).toEqual(initialState);
  });
});
