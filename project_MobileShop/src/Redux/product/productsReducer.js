import * as types from "./actions";
import * as operation from "./operations";

const initialState = {
  isLoading: false,
  isError: null,
  product: JSON.parse(localStorage.getItem("products")) || [],
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOADING_CARDS:
      return { ...state, isLoading: true, isError: null };
    case types.LOADING_ERROR:
      return { ...state, isError: action.payload, isLoading: false };
    case types.FETCH_CARTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: null,
        product: [...state.product, ...action.payload],
      };
    case types.TOGGLE_FAVORITE:
      const favoritesUpdate = operation.toggleFavorite(
        state.product,
        action.payload
      );
      return {
        ...state,
        product: [...favoritesUpdate],
      };
    case types.ADD_TO_CART:
      const addToCartUpdate = operation.addToCart(
        state.product,
        action.payload
      );
      return {
        ...state,
        product: [...addToCartUpdate],
      };
    case types.REMOVE_ONE_CART:
      const removeOneCartUpdate = operation.removeOneCart(
        state.product,
        action.payload
      );
      return {
        ...state,
        product: [...removeOneCartUpdate],
      };
    case types.REMOVE_PRODUCT_CART:
      const removeProductCartUpdate = operation.removeProductCart(
        state.product,
        action.payload
      );
      return {
        ...state,
        product: [...removeProductCartUpdate],
      };
    case types.CLEAR_CART:
      const clearProductCart = operation.clearCart(
        state.product,
        action.payload
      );
      return {
        ...state,
        product: [...clearProductCart],
      };
    default:
      return state;
  }
};
