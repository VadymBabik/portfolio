import { Modal } from "./Modal";
import { fireEvent, render } from "@testing-library/react";

describe("Test for Modal", () => {
  test("Smoke component footer", () => {
    render(<Modal />);
  });
  test("Actions  for modal", () => {
    const actionsTest = ["actionsTest"];
    const { getByText } = render(<Modal actions={actionsTest} />);
    getByText(actionsTest);
  });
  test("Show header for modal", () => {
    const headerTest = "headerTest";
    const { getByText } = render(<Modal header={headerTest} />);
    getByText(headerTest);
  });
  test("Show textModal for modal", () => {
    const textModalTest = "textModalTest";
    const { getByText } = render(<Modal textModal={textModalTest} />);
    getByText(textModalTest);
  });
  test("Test working function isOpen ", () => {
    const onClickFn = jest.fn();
    const { getByTestId } = render(<Modal isOpen={onClickFn} />);
    expect(onClickFn).not.toHaveBeenCalled();
    fireEvent.click(getByTestId("modal"));
    expect(onClickFn).toHaveBeenCalledTimes(1);
  });
  test("Test working closeButton ", () => {
    const { getByTestId } = render(<Modal closeButton={true} />);
    expect(getByTestId("modalCloseButton")).toBeInTheDocument();
  });
  test("Test working function stopPropagation ", () => {
    const { getByTestId } = render(<Modal />);
    const modalBody = getByTestId("modalBody");
    expect(modalBody).toBeInTheDocument();
    fireEvent.click(modalBody);
  });
  test("Test working function closeButton ", () => {
    const closeButton = true;
    const { getByTestId } = render(<Modal closeButton={closeButton} />);
    expect(getByTestId("modalHeader")).toBeInTheDocument();
  });
  test("Snapshot test for modal", () => {
    const { container } = render(<Modal />);
    expect(container.innerHTML).toMatchSnapshot();
  });
});
