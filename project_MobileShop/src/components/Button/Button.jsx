import React from "react";
import PropTypes from "prop-types";

export const Button = ({ click, color, icon, text }) => {
  return (
    <button
      data-testid="button"
      onClick={click}
      className={`${color} waves-effect btn`}
    >
      {icon}
      {text}
    </button>
  );
};
Button.propTypes = {
  click: PropTypes.func,
  color: PropTypes.string,
  icon: PropTypes.object,
  text: PropTypes.string,
};
Button.defaultProps = {
  icon: null,
  click: null,
};
