import React from "react";
import style from "./List.module.scss";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import { Button } from "../Button/Button";
import { useDispatch, useSelector } from "react-redux";
import {
  idModalProduct,
  toggleShowModal,
} from "../../Redux/modal/action-creators";
import { addInCart } from "../../Redux/product/action-creators";

export default function List({ productList }) {
  const dispatch = useDispatch();
  const isModal = useSelector((state) => state.modal.isModal);
  const modalProduct = useSelector((state) => state.modal.idModalProduct);

  const modalActive = (id) => {
    dispatch(idModalProduct(id));
    dispatch(toggleShowModal());
  };
  return (
    <div className={`${style.wrapper} container`}>
      {productList.map((product) => {
        return (
          <Card
            product={product}
            key={product.id}
            isFavorite={product.isFavourite}
            modal={modalActive}
          />
        );
      })}
      {isModal && (
        <Modal
          isOpen={modalActive}
          header={"Add to cart"}
          closeButton={true}
          textModal={"Are you sure you want to add this item to your cart?"}
          actions={
            <div className={style.modalFooter}>
              <Button
                size={"l"}
                click={() => {
                  dispatch(addInCart(modalProduct));
                  modalActive();
                }}
                color={"orange accent-3"}
                text={"OK"}
              />
              <Button
                size={"l"}
                click={modalActive}
                color={"orange accent-3"}
                text={"Cancel"}
              />
            </div>
          }
        />
      )}
    </div>
  );
}

