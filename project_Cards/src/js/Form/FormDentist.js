import {info} from "../utils/info";
import Input from "../componets/Input";
import Form from "./Form.js";
import {createCard} from "../CardAPI";
import renderCards from "../renderCards";
import {filterSelect} from "../Filter";
import {getCards} from "../CardAPI";

export default class FormDentist extends Form {
    constructor(doctor) {
        super (doctor)
        this.doc = new Input(info.dentist, "form-control", 'doctor').create();
        this.lastDateVisit = new Input(info.lastDateVisit, "form-control", 'lastDateVisit').create();
    }
    render(modal) {
        super.render(modal);
        this.lastDateVisit.setAttribute('data-placeholder', 'Last Visit Date');
        this.self.classList.add('form-dentist');
        this.doc.defaultValue = 'Dentist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.lastDateVisit, this.submit);
        this.send()
    }

    renderEdit(modal, id) {
        super.renderEdit(modal, id);
        this.doc.defaultValue = 'Dentist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.lastDateVisit, this.submit);
    }

    send() {
        const readData = document.querySelector('.form-doctor');
        readData.addEventListener('submit', (e) => {
            e.preventDefault();

            const formData = new FormData(e.target);
            const data = Array.from(formData.entries()).reduce((memo, pair) => ({
                ...memo,
                [pair[0]]: pair[1],
            }), {});
            createCard(data)
                .then((response) => {
                    if (response.ok) {
                        const cardWrapper = document.querySelector('.cardWrapper')
                        cardWrapper.innerHTML = "";
                        filterSelect(getCards())
                    } else {
                        throw new Error('Something went wrong');
                    }

                })
            readData.reset();
            document.querySelector(".closeBtn").click();

        });
    }
}

