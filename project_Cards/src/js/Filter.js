import {VisitCardiologist, VisitDentist, VisitTherapist} from "./Visit";
import {info} from "./utils/info";
import {GHE} from "./newEntry";
import {getCards} from "./CardAPI";

const cardWrapper = document.querySelector('.cardWrapper')

export function Filter(elem) {
    const element = GHE(elem);
    let html = "";
    html += `
    <div class="filter-search">
    <label>Search</label>
    <input class="form-control"  id="filter-search" type="search" placeholder="Search ..."/>
  </div>`;
    html += `
    <div class="filter-priority">
    <label>Priority</label>
<select class="form-select" id="filter-priority">
${info.priority.map((option) => `<option>${option}</option>`)}
</select>
  </div>`;
    html += `
    <div class="filter-status">
    <label>Status</label>
<select class="form-select"  id="filter-status">
${info.status.map((option) => `<option>${option}</option>`)}
</select>
  </div>`;
    element.innerHTML = html;

    filterSelect(getCards())
}

export function filterSelect(resArr) {
    const filterSearch = GHE('#filter-search')
    const filterPriority = GHE('#filter-priority')
    const filterStatus = GHE('#filter-status')

    resArr.then(response => response.json())
        .then(card => {
            renderFilterCards(card, cardWrapper)



                // console.log(card)
                function getInput() {
                    let currentTime = new Date();
                    const searchArr = card.filter(card => card.desc.toLowerCase().includes(filterSearch.value.toLowerCase()) || card.purpose.toLowerCase().includes(filterSearch.value.toLowerCase())|| card.fullName.toLowerCase().includes(filterSearch.value.toLowerCase()))
                    const priorityArr = searchArr.filter(function (item) {
                        if (filterPriority.value === 'Choose urgency') {
                            return item
                        } else {
                            return item.priority === filterPriority.value
                        }
                    })

                    if (filterStatus.value === 'Open') {
                        const currentArr = priorityArr.filter(card => new Date(card.date) > currentTime)
                        renderFilterCards(currentArr, cardWrapper)
                    }
                    if (filterStatus.value === 'Closed') {
                        const arr2Filrt = priorityArr.filter(card => new Date(card.date) < currentTime)
                        renderFilterCards(arr2Filrt, cardWrapper)
                    }
                    if (filterStatus.value === 'Choose status') {
                        const arr2Filrt = priorityArr.filter(card => card)
                        renderFilterCards(arr2Filrt, cardWrapper)
                    }
                }

                filterPriority.addEventListener('change', () => {
                    getInput()
                })

                filterStatus.addEventListener('change', () => {
                    getInput()
                })

                filterSearch.addEventListener('input', () => {
                    getInput()

                })
            }
        )
}

function renderFilterCards(arr, cardWrapper) {
    cardWrapper.textContent = ''
    if (arr.length === 0) {
        const emptyBase = document.createElement('div');
        emptyBase.classList.add('no-items')
        emptyBase.innerText = "No items have been added";
        cardWrapper.append(emptyBase);
    } else {
        arr.forEach(item => {
            let {doctor} = item;
            if (doctor === "Cardiologist") {
                let newCard = new VisitCardiologist(item);
                newCard.render(cardWrapper);
            } else if (doctor === "Dentist") {
                let newCard = new VisitDentist(item);
                newCard.render(cardWrapper);
            } else if (doctor === "Therapist") {
                let newCard = new VisitTherapist(item);
                newCard.render(cardWrapper);
            }
        })
        // document.querySelector('.start__text').remove();
    }
}
